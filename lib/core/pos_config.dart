class POSConfig {
  //Tabitas
  static const String androidAppId = 'com.foodhere.pos';
  static const String iOSAppId = '1280283241';
  static const String googleMapsApiKey_android = 'AIzaSyDYU2J43B4nYoB7JE1gwOXOKqv4PuyKG4s';
  static const String googleMapsApiKey_iOS = 'AIzaSyAi1yPX6E9PYTRiEV3tNeBGRfDmiQfV_w4';

  static const Map<String, String> statusName = {
    'waiting_sp_validation' : 'En espera',
    'accepted' : 'Recibido',
    'preparation' : 'En cocina',
    'incident' : 'Incidencia',
    'ready' : 'Preparado',
    'cancelled' : 'Cancelado',
    'delivering' : 'En trayecto',
    'finished' : 'Finalizado',
  };
}