import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/printer.dart';


class PrinterService extends ChangeNotifier {

  Printer printer;

  Stream<QuerySnapshot> getPrintersStream(String businessID, List<dynamic> queries) {

    DocumentReference businessRef = Firestore.instance.collection('business').document(businessID);
    CollectionReference ordersRef = businessRef.collection('orders');

    Query filterQuery = ordersRef;

    if (queries != null) {
      queries.forEach((query) {
        switch (query['op']) {
          case 'isEqualTo':
            filterQuery =
                filterQuery.where(query['case'], isEqualTo: query['data']);
            break;
        }
      });
    }

    return filterQuery.snapshots();
  }

  Future<PosPrintResult> printTicket(Ticket ticket, String printerIP) async {

    final PrinterNetworkManager printerManager = PrinterNetworkManager();
    printerManager.selectPrinter(printerIP);
    ticket.cut();

    return printerManager.printTicket(ticket);
  }

}
