import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/store.dart';

class StoreService extends ChangeNotifier {

  final Firestore firestore = Firestore.instance;

  Store _store;

  Store get store => _store;

  set store(Store store) {
    this._store = store;
  }

  Future<DocumentSnapshot> getStore(String businessId, String storeId) async {
   final storeRef = firestore.collection('business').document(businessId).collection('stores').document(storeId);
   return storeRef.get();
  }

}