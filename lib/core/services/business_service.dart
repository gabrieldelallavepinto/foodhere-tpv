import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/business.dart';

class BusinessService extends ChangeNotifier {

  Business _business;

  Business get business => _business;

  set business(Business business) {
    this._business = business;
  }

  Future<Business> getBusiness(String businessID) async {
   var doc = await Firestore.instance.collection('business').document(businessID).get();

   return Business.fromMap(doc.data);
  }

  Stream<QuerySnapshot> fetchBusinessAsStream() {
    return Firestore.instance.collection('business').snapshots();
  }

  Future<dynamic> prueba() async {
    print('entra en prueba');
    return 'prueba';
  }


}