import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/order_.dart';
import 'package:foodherepos/core/services/printer_service.dart';

class OrderService extends ChangeNotifier {
  List<Order> orders;
  Order order;

  Stream<QuerySnapshot> getOrdersStream(String businessID, {String storeId}) {

    CollectionReference ordersRef = Firestore.instance.collection('business').document(businessID).collection('orders');

    Query ordersQuery = ordersRef;
    if(storeId != null) ordersQuery = ordersQuery.where('store.id', isEqualTo: storeId);

    return ordersQuery.snapshots();
  }

  Future<QuerySnapshot> getOrders(String businessID, {String storeId}) {

    CollectionReference ordersRef = Firestore.instance.collection('business').document(businessID).collection('orders');

    Query ordersQuery = ordersRef;
    if(storeId != null) ordersQuery = ordersQuery.where('store.id', isEqualTo: storeId);

    return ordersQuery.getDocuments();

  }

  printOrder(Order order) async {
    Ticket ticket = await order.toTicket();

    await PrinterService().printTicket(ticket, '192.168.1.100');
  }

  // Estos método son los que uso para obtener los pedidos

  Future<Order> getOrderById({String id}) async {

    var doc = await Firestore.instance
        .collection('business')
        .document('zLIzDEFA1hVwXKWLCxLR')
        .collection('orders')
        .document(id)
        .get();
    var order = Order.fromMap(doc.data);
    return order;
  }

  Stream<QuerySnapshot> fetchOrdersAsStream({String status}) {

    CollectionReference ref = Firestore.instance
        .collection('business')
        .document('zLIzDEFA1hVwXKWLCxLR')
        .collection('orders');

    return ref.where('status', isEqualTo: status).snapshots();
  }
}


