import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/business.dart';
import 'package:foodherepos/core/models/employee.dart';

class EmployeeService extends ChangeNotifier {

  final Firestore _firestore = Firestore.instance;

  Future<Employee> getEmployee(String businessID, String employeeID) async {
    final employeeRef = Firestore.instance
        .collection('business')
        .document(businessID)
        .collection('employees')
        .document(employeeID);
    final doc = await employeeRef.get();

    return Employee.fromMap(doc.data);
  }

  Future<Employee> getEmployeeGroupCollection(String employeeID) async {

    Query employeesRef = _firestore.collectionGroup('employees').where('id', isEqualTo: employeeID).limit(1);

    final snapshot = await employeesRef.getDocuments();

    if(snapshot.documents.isNotEmpty){

      DocumentSnapshot doc = snapshot.documents.first;
      print(doc.data);

      final employee = Employee.fromMap(doc.data);
      print(employee.toJson());
      return employee;

    }else{
      return null;
    }

  }

}
