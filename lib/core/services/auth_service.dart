import 'package:firebase_auth/firebase_auth.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/employee.dart';
import 'package:foodherepos/core/services/employee_service.dart';

class AuthService extends ChangeNotifier {

  final Firestore firestore = Firestore.instance;

  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser _user;
  Employee _employee;

  AuthService() {
    _auth.onAuthStateChanged.listen(_onAuthStateChanged);
  }

  Employee _userFromFirebase(FirebaseUser user) {
    return user != null ? Employee(id: user.uid) : null;
  }

  FirebaseUser get user => _user;
  Employee get employee => _employee;

  Future<void> _onAuthStateChanged(FirebaseUser firebaseUser) async {

    print('_onAuthStateChanged');

    if(firebaseUser == null){
      print('_onAuthStateChanged: no estoy logueado');
      _user = null;
    }else{
      _user = firebaseUser;
      print('_onAuthStateChanged: estoy logueado');

      Employee employee = await EmployeeService().getEmployeeGroupCollection(_user.uid);

      if(employee == null){
        _employee = null;
        this.signOut();
      }else{
        _employee = employee;
      }

    }

    notifyListeners();
  }

  // auth change user stream
  void setEmployee(String id) async {

    if(id == null){
      print('no hay usuario autentificado');
    }else{
      Query employeesRef = firestore.collectionGroup('employees').where('id', isEqualTo: id).limit(1);

      employeesRef.snapshots().listen((data) {
        if(data.documents.isNotEmpty){
          DocumentSnapshot snapshot = data.documents.first;
          print(snapshot.data);
          _employee = Employee.fromMap(snapshot.data);
          print(employee.toJson());
          notifyListeners();

        }else{
          print('no se encuentra ningún empleado');
        }
      });

    }

  }




  // sing in with email and password
  Future signIn(String email, String password) async {
    try{
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return user;
    }catch(e) {
      print(e.toString());
      return null;
    }
  }

  // sing in with email and password
  Future signOut() async {
    try{
      await _auth.signOut();
    }catch(e) {
      print(e.toString());
      return null;
    }
  }


}
