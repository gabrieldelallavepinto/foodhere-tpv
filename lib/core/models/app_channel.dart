class AppChannel {
  bool enabled;
  String id;
  String minVersion;
  String version;
  String name;

  AppChannel({this.id, this.name, this.enabled, this.minVersion, this.version});

  AppChannel.fromMap(Map snapshot)
      : enabled = snapshot['enabled'] ?? false,
        id = snapshot['id'] ?? null,
        name = snapshot['name'] ?? null,
        minVersion = snapshot['min_version'] ?? null,
        version = snapshot['version'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "min_version": minVersion,
      "version": version,
      "name": name
    };
  }
}
