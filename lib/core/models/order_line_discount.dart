class OrderLineDiscount {
  String id;
  int sinqroId;

  OrderLineDiscount({this.id, this.sinqroId});

  toJson() {
    return {'id': id, 'sinqro_id': sinqroId};
  }
}
