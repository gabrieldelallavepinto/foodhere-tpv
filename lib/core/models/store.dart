import 'order_type.dart';
import 'address.dart';

class Store {

  bool active;
  Address address;
  String businessId;
  String email;
  String id;
  List<dynamic> images;
  String name;
  String optionalPhone;
  Map<String, dynamic> orderSchedule;
  OrderTypes orderTypes;
  Map<String, dynamic> paymentMethods;
  String phone;
  String scheduleLabel;
  String website;
  String description;
  String type;
  String status;
  String private_email;
  String private_phone;
  double distance;

  Store({
    this.active,
    this.address,
    this.businessId,
    this.email,
    this.id,
    this.images,
    this.name,
    this.optionalPhone,
    this.orderSchedule,
    this.orderTypes,
    this.paymentMethods,
    this.phone,
    this.scheduleLabel,
    this.website,
    this.description,
    this.type,
    this.status,
    this.private_email,
    this.private_phone,
    this.distance,

  });

  Store.fromMap(Map snapshot)
      :
        id = snapshot['store_id'] ?? null,
        active = snapshot['active'] ?? false,
        status = snapshot['status'] ?? 'closed',
        name = snapshot['name'] ?? '',
        address = Address.fromMap(snapshot['address']) ?? null,
        businessId = snapshot['business_id'] ?? '',
        description = snapshot['description'] ?? null,
        email = snapshot['email'] ?? '',
        images = snapshot['images'] ?? '',
        optionalPhone = snapshot['optional_phone'] ?? '',
      
        orderSchedule = snapshot['order_schedule'] ?? null,
        orderTypes = OrderTypes.fromMap(snapshot['order_types']) ?? null,
  //paymentMethods = snapshot['payment_methods'] ?? null,
        phone = snapshot['phone'] ?? '',
        scheduleLabel = snapshot['schedule_label'] ?? '',

        website = snapshot['website'] ?? ''
  ;

  toJson() {
    return {
      "active": active,
      "address": address.toJson(),
      "business_id": businessId,
      "email": email,
      "id": id,
      "images" : images,
      "name": name,
      "optional_phone" : optionalPhone,
      "order_schedule" : orderSchedule,
      "order_types": orderTypes.toJson(),
      //"payment_methods" : paymentMethods,
      "phone": phone,
      "schedule_label": scheduleLabel,
      "website": website,
      "distance" : distance
    };
  }

}

enum StatusEnum {
  enabled,
  disabled,
}