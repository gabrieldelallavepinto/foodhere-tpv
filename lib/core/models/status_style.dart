import 'package:flutter/material.dart';

class StatusStyle {
  String status;
  String label;
  Color color;
  Color textColor;

  StatusStyle({this.textColor, this.label, this.color, this.status});
}