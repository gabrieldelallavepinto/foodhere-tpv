import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';

class Printer {
  String id;
  String type;
  String name;
  String ip;

  Printer({
    this.id,
    this.type,
    this.name,
    this.ip,
  });

  Printer.fromMap(Map snapshot):
        id = snapshot['id'],
        type = snapshot['type'],
        name = snapshot['name'],
        ip = snapshot['ip'];

  toJson() {
    return {
      'id': id,
      'type': type,
      'name': name,
      'ip': ip,
    };
  }

  testTicket() async {
    Ticket ticket = Ticket(PaperSize.mm80);

    ticket.text("PRUEBA DE IMPRESIÓN", styles: PosStyles(bold: true, align: PosAlign.center));

    ticket.hr();

    ticket.text("Tipo: ${this.type}", styles: PosStyles(bold: true, align: PosAlign.left));
    ticket.text("Nombre: ${this.name}", styles: PosStyles(bold: true, align: PosAlign.left));
    ticket.text("IP: ${this.ip}", styles: PosStyles(bold: true, align: PosAlign.left));

    return ticket;
  }

  toPrint({Ticket ticket}) async {

    final PrinterNetworkManager printerManager = PrinterNetworkManager();
    printerManager.selectPrinter(this.ip);

    ticket.cut();
//    return await printerManager.printTicket(ticket);
  }

}
