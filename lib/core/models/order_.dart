import 'dart:async';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:intl/intl.dart';
import 'package:network_image_to_byte/network_image_to_byte.dart';
import 'address.dart';
import 'client.dart';
import 'order_line.dart';
import 'store.dart';
import 'package:image/image.dart' as Img;

class Order {
  String id;
  String ref;
  bool printed;
  dynamic sinqro_id;
  double totalAmount;
  String executionTimeLabel;
  Timestamp executionTime;
  String comment;
  String cancellationComment; // Sólo si es cancelado
  Client client;
  String status;
  Timestamp date;
  String type; //delivery, collection, insitu, reservation
  Store store;
  List<dynamic> payments;
  List<OrderLine> orderLines;
  dynamic deliveryAmount;
  Address deliveryAddress;
  List<dynamic> deliveries;
  dynamic dinersNumber;
  bool isCancellable;
  String posOrderId;
  String marketOrderId;
  String marketOrderCode;
  String business_id;
  String store_id;

  Order(
      {this.id,
      this.sinqro_id,
      this.ref,
      this.printed,
      this.totalAmount,
      this.executionTimeLabel,
      this.executionTime,
      this.comment,
      this.cancellationComment,
      this.client,
      this.status,
      this.date,
      this.type,
      this.store,
      this.payments,
      this.orderLines,
      this.deliveryAmount,
      this.deliveryAddress,
      this.deliveries,
      this.dinersNumber,
      this.isCancellable,
      this.posOrderId,
      this.marketOrderId,
      this.marketOrderCode,
      this.business_id,
      this.store_id});

  Order.fromMap(Map snapshot)
      : id = snapshot['id'],
        sinqro_id = snapshot['sinqro_id'],
        ref = snapshot['ref'] ?? null,
        printed = snapshot['printed'] ?? false,
        business_id = snapshot['business_id'] ?? null,
        store_id = snapshot['store_id'] ?? null,
        totalAmount = snapshot['total_amount'],
        executionTimeLabel = snapshot['execution_time_label'],
        executionTime = snapshot['execution_time'],
        comment = snapshot['comment'],
        cancellationComment = snapshot['cancellation_comment'],
        client = Client.fromMap(snapshot['client']),
        status = snapshot['status'],
        date = snapshot['date'],
        type = snapshot['type'],
        orderLines = _orderLines(snapshot['order_lines']),
        store = Store.fromMap(snapshot['store']),
        payments = snapshot['payments'],
        deliveryAmount = snapshot['delivery_amount'],
        deliveryAddress = snapshot['delivery_address'] == null
            ? null
            : Address.fromMap(snapshot['delivery_address']),
        deliveries = snapshot['deliveries'],
        dinersNumber = snapshot['diners_number'],
        isCancellable = snapshot['is_cancellable'],
        posOrderId = snapshot['pos_order_id'],
        marketOrderId = snapshot['market_order_id'],
        marketOrderCode = snapshot['market_order_code'];

  toJson() {
    return {
      'order_lines': orderLines != null
          ? orderLines.map((e) => e.toJson()).toList()
          : null,
      'id': id,
      'id_sinqro': sinqro_id,
      'ref': ref,
      'total_amount': totalAmount,
      'execution_time_label': executionTimeLabel,
      'execution_time': executionTime,
      'comment': comment,
      'cancellation_comment': cancellationComment,
      'client': client.toJson(),
      'status': status,
      'date': date,
      'type': type,
      'payments': payments,
      'delivery_amount': deliveryAmount,
      'delivery_address':
          deliveryAddress != null ? deliveryAddress.toJson() : null,
      'deliveries': deliveries,
      'diners_number': dinersNumber,
      'is_cancellable': isCancellable,
      'pos_order_id': posOrderId,
      'market_order_id': marketOrderId,
      'market_order_code': marketOrderCode,
      'store': store.toJson(),
    };
  }

  toTicket({Ticket ticket}) async {
    if(ticket == null) ticket = Ticket(PaperSize.mm80);

    double taxBase = 0.00;
    dynamic tax = {'21.00': 0.00, '10.00': 0.00, '4.00': 0.00};

    Uint8List byteImage = await networkImageToByte('https://cybersur.net/wp-content/uploads/2015/08/tabitas.jpg');
    Img.Image image = Img.decodeImage(byteImage);

    ticket.image(image);
    ticket.text(this.store.name, styles: PosStyles(bold: true, align: PosAlign.center, codeTable: PosCodeTable.westEur));
    ticket.text("${this.store.address.formattedAddress}", styles: PosStyles(bold: true, align: PosAlign.center, codeTable: PosCodeTable.westEur));

    ticket.feed(1);

    ticket.text("Fecha: ${new DateFormat('d/M/y').format(this.date.toDate())}", styles: PosStyles(bold: false, align: PosAlign.left, codeTable: PosCodeTable.westEur));
    ticket.text("Factura: ${this.ref}", styles: PosStyles(bold: false, align: PosAlign.left));

    if(this.orderLines != null){
      ticket.hr();

      ticket.row([
        PosColumn(
          text: 'Uds',
          width: 1,
          styles: PosStyles(align: PosAlign.center),
        ),
        PosColumn(
          text: 'Descripción',
          width: 7,
          styles: PosStyles(align: PosAlign.left, codeTable: PosCodeTable.westEur),
        ),
        PosColumn(
          text: 'Precio',
          width: 2,
          styles: PosStyles(align: PosAlign.right),
        ),
        PosColumn(
          text: 'Total',
          width: 2,
          styles: PosStyles(align: PosAlign.right),
        ),
      ]);

      ticket.hr();

      await Future(() async => {

        this.orderLines.forEach((orderLine) async {

          ticket = await orderLine.toTicket(ticket: ticket);

          switch(orderLine.tax.toStringAsFixed(2)){

          case '21.00':
          final tb = orderLine.totalAmount / 1.21;
          taxBase += tb;
          tax['21.00'] += orderLine.totalAmount - tb;
          break;

          case '10.00':
          final tb = orderLine.totalAmount / 1.10;
          taxBase += tb;
          tax['10.00'] += orderLine.totalAmount - tb;
          break;

          case '4.00':
          final tb = orderLine.totalAmount / 1.04;
          taxBase += tb;
          tax['4.00'] += orderLine.totalAmount - tb;
          break;
          }

        })

      });

      ticket.hr();

      ticket.text("Base Imp: ${taxBase.toStringAsFixed(2)}", styles: PosStyles(bold: false, align: PosAlign.right, codeTable: PosCodeTable.westEur));

      if(tax['21.00'] != 0.00) ticket.text("Iva 21%: ${tax['21.00'].toStringAsFixed(2)}", styles: PosStyles(bold: false, align: PosAlign.right, codeTable: PosCodeTable.westEur));
      if(tax['10.00'] != 0.00) ticket.text("Iva 10%: ${tax['10.00'].toStringAsFixed(2)}", styles: PosStyles(bold: false, align: PosAlign.right, codeTable: PosCodeTable.westEur));
      if(tax['4.00'] != 0.00) ticket.text("Iva 4%: ${tax['4.00'].toStringAsFixed(2)}", styles: PosStyles(bold: false, align: PosAlign.right, codeTable: PosCodeTable.westEur));

      ticket.text("TOTAL: ${this.totalAmount.toStringAsFixed(2)}", styles: PosStyles(bold: false, height: PosTextSize.size2, width: PosTextSize.size2, align: PosAlign.right, codeTable: PosCodeTable.westEur));

      ticket.hr();
    }


    ticket.text("Entrega: ${this.type}", styles: PosStyles(bold: false, align: PosAlign.left));

    if(this.type == 'delivery'){
      ticket.text("Dirección: ${this.deliveryAddress.addressLine1}, ${this.deliveryAddress.addressLine2}, ${this.deliveryAddress.postalCode}, ${this.deliveryAddress.locality}, ${this.deliveryAddress.state}", styles: PosStyles(bold: false, align: PosAlign.left));
    }

    ticket.feed(2);
    ticket.text("GRACIAS POR SU VISITA", styles: PosStyles(bold: false, align: PosAlign.center));

    return ticket;
  }

  static List<OrderLine> _orderLines(List<dynamic> orderLines) {
    if (orderLines == null) {
      return null;
    } else {
      return orderLines.map((e) => OrderLine.fromMap(e)).toList();
    }
  }
}
