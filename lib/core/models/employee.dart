import 'package:cloud_firestore/cloud_firestore.dart';

class Employee {
  String avatarUrl;
  Timestamp birthdate;
  String businessId;
  String storeId;
  String customerId;
  String email;
  String firstName;
  String id;
  String gender;
  String lastName;
  String membershipCode;
  String phone;

  Employee({
    this.avatarUrl,
    this.birthdate,
    this.businessId,
    this.storeId,
    this.customerId,
    this.email,
    this.firstName,
    this.gender,
    this.id,
    this.lastName,
    this.membershipCode,
    this.phone,
  });

  Employee.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        storeId = snapshot['store_id'] ?? null,
        avatarUrl = snapshot['avatar_url'] ?? null,
        birthdate = snapshot['birthdate'] ?? null,
        customerId = snapshot['customer_id'] ?? null,
        email = snapshot['email'] ?? null,
        firstName = snapshot['first_name'] ?? null,
        gender = snapshot['gender'] ?? null,
        lastName = snapshot['last_name'] ?? null,
        membershipCode = snapshot['memberchip_code'] ?? null,
        phone = snapshot['phone'] ?? null;

  toJson() {
    return {
      "id": this.id,
      "business_id": this.businessId,
      "store_id": this.storeId,
      "avatar_url": this.avatarUrl,
      "birthdate": this.birthdate,
      "customer_id": this.customerId,
      "email": this.email,
      "first_name": this.firstName,
      "gender": this.gender,
      "last_name": this.lastName,
      "membership_code": this.membershipCode,
      "phone": this.phone
    };
  }
}
