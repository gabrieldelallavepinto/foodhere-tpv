import 'address.dart';

class Business {
  String id;
  bool active;
  String status;
  String domain;
  String name;
  String businessName;
  String documentation;
  String phone;
  String optionalPhone;
  Address address;
  String website;
  dynamic images;

  Business(
      {this.id,
      this.active,
      this.status,
      this.domain,
      this.name,
        this.businessName,
        this.documentation,
        this.optionalPhone,
        this.phone,
        this.address,
      this.website,
      this.images});

  Business.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        active = snapshot['active'] ?? null,
        status = snapshot['status'] ?? null,
        domain = snapshot['domain'] ?? null,
        name = snapshot['name'] ?? null,
        businessName = snapshot['business_name'] ?? null,
        documentation = snapshot['documentation'] ?? null,
        phone = snapshot['phone'] ?? null,
        optionalPhone = snapshot['optional_phone'] ?? null,
        address = Address.fromMap(snapshot['address']) ?? null,
        website = snapshot['website'] ?? null,
        images = snapshot['images'];

  toJson() {
    return {
      "id": id,
      "active": active,
      "status": status,
      "domain": domain,
      "name": name,
      "business_name": businessName,
      "documentation": documentation,
      "phone": phone,
      "optional_phone": optionalPhone,
      "address": address,
      "website": website,
      "images": images
    };
  }
}
