import 'package:flutter/material.dart';

class MenuItem {
  String name;
  String icon;
  String route;
  String description;
  Color color;
  List<SettingOption> settingOptions;

  MenuItem({this.name, this.icon, this.route, this.color, this.description, this.settingOptions});
}

class SettingOption {
  String name;
  String icon;
  String description;
  String route;

  SettingOption({this.name, this.icon, this.description, this.route});
}
