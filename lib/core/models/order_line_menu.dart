class OrderLineMenu {
  String id;
  int sinqroId;

  OrderLineMenu({this.id, this.sinqroId});

  OrderLineMenu.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        sinqroId = snapshot['sinqro_id'] ?? null;

  toJson() {
    return {'id': id, 'sinqro_id': sinqroId};
  }
}
