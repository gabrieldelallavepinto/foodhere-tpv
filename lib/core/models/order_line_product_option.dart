class OrderLineProductOption {
  String id;
  int sinqroId;

  OrderLineProductOption({this.id, this.sinqroId});

  OrderLineProductOption.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        sinqroId = snapshot['sinqro_id'] ?? null;

  toJson() {
    return {'id': id, 'sinqro_id': sinqroId};
  }
}
