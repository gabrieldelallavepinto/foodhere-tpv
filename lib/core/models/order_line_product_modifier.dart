class OrderLineProductModifier {
  String id;
  int sinqroId;

  OrderLineProductModifier({this.id, this.sinqroId});

  OrderLineProductModifier.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        sinqroId = snapshot['sinqro_id'] ?? null;

  toJson() {
    return {'id': id, 'sinqro_id': sinqroId};
  }
}
