class OrderTypes {
  OrderType collect;
  OrderType delivery;
  OrderType inStore;

  OrderTypes.fromMap(Map snapshot)
      : collect = OrderType.fromMap(snapshot['collect']) ?? null,
        delivery = OrderType.fromMap(snapshot['delivery']) ?? null,
        inStore = OrderType.fromMap(snapshot['in_store']) ?? null;

  toJson() {
    return {
      "collect": collect.toJson(),
      "delivery": delivery.toJson(),
      "in_store": inStore.toJson()
    };
  }
}

class OrderType {
  bool enabled;
  String id;
  double minAmount;
  int preparationTime;
  List<dynamic> zones;
  int maxDiners;
  List<dynamic> paymentMethods;

  OrderType(
      {this.enabled,
      this.id,
      this.minAmount,
      this.preparationTime,
      this.zones,
      this.maxDiners,
      this.paymentMethods});

  OrderType.fromMap(Map snapshot)
      : enabled = snapshot['enabled'] ?? null,
        id = snapshot['id'] ?? null,
        minAmount = double.parse(snapshot['min_amount'].toString()) ?? null,
        preparationTime =
            int.parse(snapshot['preparation_time'].toString()) ?? null,
        maxDiners = snapshot['max_diners'] != null
            ? int.parse(snapshot['max_diners'].toString())
            : null,
        paymentMethods = snapshot['payment_methods'] ?? null,
        zones = snapshot['zones'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "min_amount": minAmount,
      "max_diners": maxDiners,
      "preparation_time": preparationTime,
      "payment_methods": paymentMethods,
      "zones": zones
    };
  }
}

class DeliveryZone {
  double deliveryAmount;
  String postalCode;

  DeliveryZone({this.deliveryAmount, this.postalCode});

  DeliveryZone.fromMap(Map snapshot)
      : deliveryAmount =
            double.parse(snapshot['delivery_amount'].toString()) ?? null,
        postalCode = snapshot['postal_code'] ?? null;

  toJson() {
    return {"delivery_amount": deliveryAmount, "postalCode": postalCode};
  }
}
