import 'dart:async';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:intl/intl.dart';
import 'package:network_image_to_byte/network_image_to_byte.dart';
import 'address.dart';
import 'client.dart';
import 'order_line.dart';
import 'store.dart';
import 'package:image/image.dart' as Img;

class Order {
  String id;
  String ref;
  String status;
  Timestamp date;
  Address deliveryAddress;
  List<dynamic> orderLines;
  bool printed;
  Store store;
  double totalAmount;
  String type;
  Client client;

  Order(this.id, this.ref, this.status, this.date, this.orderLines,
      this.printed, this.store, this.totalAmount, this.type, this.client);

  Order.fromMap(Map snapshot) {
    this.id = snapshot['id'] ?? null;
    this.ref = snapshot['ref']?? null;
    this.status = snapshot['status'] ?? null;
    this.date = snapshot['date'] ?? null;
    this.deliveryAddress = snapshot['delivery_address'] != null ? Address.fromMap(snapshot['delivery_address']) : null;
    this.orderLines = snapshot['order_lines'] != null ? snapshot['order_lines'].map((orderLine) => OrderLine.fromMap(orderLine)).toList() : null;
    this.printed = snapshot['printed'] ?? null;
    this.store = Store.fromMap(snapshot['store']) ?? null;
    this.totalAmount = snapshot['total_amount'] ?? null;
    this.type = snapshot['type'] ?? null;
    this.client = Client.fromMap(snapshot['client']) ?? null;
  }

  toJson() {
    return {
      'id': this.id,
      'ref': this.ref,
      'status': this.status,
      'date': this.date,
      'delivery_address': this.deliveryAddress,
      'orderLines': this.orderLines,
      'printed': this.printed,
      'store': this.store,
      'total_amount': this.totalAmount,
      'type': this.type,
      'client': this.client.toJson(),
    };
  }

  Future<Ticket> toTicket({Ticket ticket}) async {
    if (ticket == null) ticket = Ticket(PaperSize.mm80);

    double taxBase = 0.00;
    dynamic tax = {'21.00': 0.00, '10.00': 0.00, '4.00': 0.00};

    Uint8List byteImage = await networkImageToByte(
        'https://cybersur.net/wp-content/uploads/2015/08/tabitas.jpg');
    Img.Image image = Img.decodeImage(byteImage);

    ticket.image(image);
    ticket.text(this.store.name,
        styles: PosStyles(
            bold: true,
            align: PosAlign.center,
            codeTable: PosCodeTable.westEur));
    ticket.text("${this.store.address.formattedAddress}",
        styles: PosStyles(
            bold: true,
            align: PosAlign.center,
            codeTable: PosCodeTable.westEur));

    ticket.feed(1);

    ticket.text("Fecha: ${new DateFormat('d/M/y').format(this.date.toDate())}",
        styles: PosStyles(
            bold: false,
            align: PosAlign.left,
            codeTable: PosCodeTable.westEur));
    ticket.text("Factura: ${this.ref}",
        styles: PosStyles(bold: false, align: PosAlign.left));

    if (this.orderLines != null) {
      ticket.hr();

      ticket.row([
        PosColumn(
          text: 'Uds',
          width: 1,
          styles: PosStyles(align: PosAlign.center),
        ),
        PosColumn(
          text: 'Descripción',
          width: 7,
          styles:
              PosStyles(align: PosAlign.left, codeTable: PosCodeTable.westEur),
        ),
        PosColumn(
          text: 'Precio',
          width: 2,
          styles: PosStyles(align: PosAlign.right),
        ),
        PosColumn(
          text: 'Total',
          width: 2,
          styles: PosStyles(align: PosAlign.right),
        ),
      ]);

      ticket.hr();

      await Future(() async => {
            this.orderLines.forEach((orderLine) async {
              ticket = await orderLine.toTicket(ticket: ticket);

              switch (orderLine.tax.toStringAsFixed(2)) {
                case '21.00':
                  final tb = orderLine.totalAmount / 1.21;
                  taxBase += tb;
                  tax['21.00'] += orderLine.totalAmount - tb;
                  break;

                case '10.00':
                  final tb = orderLine.totalAmount / 1.10;
                  taxBase += tb;
                  tax['10.00'] += orderLine.totalAmount - tb;
                  break;

                case '4.00':
                  final tb = orderLine.totalAmount / 1.04;
                  taxBase += tb;
                  tax['4.00'] += orderLine.totalAmount - tb;
                  break;
              }
            })
          });

      ticket.hr();

      ticket.text("Base Imp: ${taxBase.toStringAsFixed(2)}",
          styles: PosStyles(
              bold: false,
              align: PosAlign.right,
              codeTable: PosCodeTable.westEur));

      if (tax['21.00'] != 0.00)
        ticket.text("Iva 21%: ${tax['21.00'].toStringAsFixed(2)}",
            styles: PosStyles(
                bold: false,
                align: PosAlign.right,
                codeTable: PosCodeTable.westEur));
      if (tax['10.00'] != 0.00)
        ticket.text("Iva 10%: ${tax['10.00'].toStringAsFixed(2)}",
            styles: PosStyles(
                bold: false,
                align: PosAlign.right,
                codeTable: PosCodeTable.westEur));
      if (tax['4.00'] != 0.00)
        ticket.text("Iva 4%: ${tax['4.00'].toStringAsFixed(2)}",
            styles: PosStyles(
                bold: false,
                align: PosAlign.right,
                codeTable: PosCodeTable.westEur));

      ticket.text("TOTAL: ${this.totalAmount.toStringAsFixed(2)}",
          styles: PosStyles(
              bold: false,
              height: PosTextSize.size2,
              width: PosTextSize.size2,
              align: PosAlign.right,
              codeTable: PosCodeTable.westEur));

      ticket.hr();
    }

    ticket.text("Entrega: ${this.type}",
        styles: PosStyles(bold: false, align: PosAlign.left));

    if (this.type == 'delivery') {
      ticket.text(
          "Dirección: ${this.deliveryAddress.addressLine1}, ${this.deliveryAddress.addressLine2}, ${this.deliveryAddress.postalCode}, ${this.deliveryAddress.locality}, ${this.deliveryAddress.state}",
          styles: PosStyles(bold: false, align: PosAlign.left));
    }

    ticket.feed(2);
    ticket.text("GRACIAS POR SU VISITA",
        styles: PosStyles(bold: false, align: PosAlign.center));

    return ticket;
  }
}
