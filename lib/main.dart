import 'package:flutter/material.dart';
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/order_service.dart';
import 'package:foodherepos/locator.dart';
import 'package:foodherepos/views/router.dart';
import 'package:provider/provider.dart';
import 'package:foodherepos/core/services/order_service.dart';

import 'core/services/store_service.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService()),
        ChangeNotifierProvider(create: (_) => BusinessService()),
        ChangeNotifierProvider(create: (_) => StoreService()),
        ChangeNotifierProvider(create: (_) => OrderService()),
      ],
      child: MaterialApp(
        theme: ThemeData(
          buttonColor: Colors.blue,
          brightness: Brightness.light,
          primaryColor: Colors.blue,
        ),
        darkTheme: ThemeData(
          buttonColor: Colors.red,
          primaryColor: Colors.red,
          brightness: Brightness.dark,
        ),
        initialRoute: 'wrapper',
        debugShowCheckedModeBanner: false,
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }
}
