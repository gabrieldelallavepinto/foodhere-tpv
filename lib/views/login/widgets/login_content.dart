import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/shared/theme.dart';
import 'package:provider/provider.dart';

class LoginContent extends StatefulWidget {
  @override
  _LoginContentState createState() => _LoginContentState();
}

class _LoginContentState extends State<LoginContent> {
  final _formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String error = '';
  bool _passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    var maxHeight = MediaQuery.of(context).size.height;
    final authService = Provider.of<AuthService>(context);
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.all(20.0),
            height: 60.00,
            child: Image.asset(
              "assets/images/logo_foodhere.png",
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(height: 15.0),
          TextFormField(
            style: TextStyle(
              color: Colors.white,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Color.fromRGBO(30, 30, 30, 0.8),
              labelText: 'Email',
              labelStyle: TextStyle(color: Colors.grey),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: BorderSide(
                    width: 2.00, color: Color.fromRGBO(255, 255, 255, 0.1)),
              ),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.grey,
              ),
            ),
            validator: (val) {
              if (val.isEmpty) return 'Introduce el email';
              return null;
            },
            onChanged: (val) {
              setState(() => email = val);
            },
          ),
          SizedBox(height: 15.0),
          TextFormField(
            obscureText: _passwordVisible ? false : true,
            style: TextStyle(
              color: Colors.white,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Color.fromRGBO(30, 30, 30, 0.8),
              labelText: 'Contraseña',
              labelStyle: TextStyle(color: Colors.grey),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: BorderSide(
                    width: 2.00, color: Color.fromRGBO(255, 255, 255, 0.1)),
              ),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.grey,
              ),
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Colors.grey,
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
              ),
            ),
            validator: (val) {
              if (val.isEmpty) {
                return 'Introduce la contraseña';
              }
              if (val.length < 6) {
                return 'La contraseña debe tener más de 6 caracteres';
              }
              return null;
            },
            onChanged: (val) {
              setState(() => password = val);
            },
          ),
          SizedBox(height: 15.0),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            height: 50.0,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 14.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text(
                'Iniciar Sesión',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  dynamic result = await authService.signIn(email, password);
                  if (result == null) {
                    setState(() {
                      error = 'Usuario o contraseña incorrecto';
                    });
                    print('error al iniciar sesión');
                  } else {
                    print('se ha iniciado sesión');
                    print(result);
                  }
                }
              },
            ),
          ),
          SizedBox(height: 20.0),
          Text(
            error,
            style: TextStyle(color: Colors.red, fontSize: 14.0),
          )
        ],
      ),
    );
  }
}
