import 'package:flutter/material.dart';

class LoginHeader extends StatefulWidget {
  @override
  _LoginHeaderState createState() => _LoginHeaderState();
}

class _LoginHeaderState extends State<LoginHeader> {
  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      minWidth: 0.0,
      minHeight: 0.0,
      maxWidth: 220.00,
      child: Image.asset(
        "assets/images/logo_foodhere.png",
        fit: BoxFit.fill,
      ),
    );
  }
}
