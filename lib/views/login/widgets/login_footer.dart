import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodherepos/views/home/widgets/clock/digital_clock.dart';

class LoginFooter extends StatefulWidget {
  @override
  _LoginFooterState createState() => _LoginFooterState();
}

class _LoginFooterState extends State<LoginFooter> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Image.asset(
              "assets/images/logo_foodhere.png",
              height: 90,
              fit: BoxFit.fitHeight,
            ),
          ),
        ],
      ),
    );
  }
}
