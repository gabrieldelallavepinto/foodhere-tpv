import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/shared/loading.dart';
import 'package:foodherepos/shared/theme.dart';
import 'package:foodherepos/views/home/home_theme.dart';
import 'package:foodherepos/views/login/widgets/login_content.dart';
import 'package:foodherepos/views/login/widgets/login_header.dart';

import 'login_theme.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Image.asset(
          "assets/images/background_2.jpg",
          height: height,
          width: width,
          fit: BoxFit.cover,
        ),

        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            color: Colors.black38.withOpacity(0.2),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          /*appBar: AppBar(
            backgroundColor: Colors.transparent,
          ),*/
          body: SafeArea(
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                  constraints: BoxConstraints(maxWidth: 700),
                  padding: EdgeInsets.symmetric(
                      vertical: 0.0, horizontal: width * 0.1),
                  child: LoginContent(),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
