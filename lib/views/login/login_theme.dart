import 'package:flutter/material.dart';

final ThemeData loginTheme = new ThemeData(
  primaryColor: Color.fromARGB(255, 229, 30, 37),
  accentColor: Color.fromARGB(255, 0, 152, 77),
  appBarTheme: AppBarTheme(brightness: Brightness.light),
  textTheme: TextTheme(
    bodyText2: TextStyle(
      color: Colors.white,
    ),
  ),
);