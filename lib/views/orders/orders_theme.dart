import 'package:flutter/material.dart';

final ThemeData ordersTheme = new ThemeData(
  primaryColor: Color.fromARGB(255, 229, 30, 37),
  accentColor: Color.fromARGB(255, 0, 152, 77),
);