import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodherepos/core/models/order.dart';
import 'package:foodherepos/core/services/order_service.dart';
import 'package:foodherepos/views/orders/orders_theme.dart';
import 'package:foodherepos/views/orders/widgets/filter_button.dart';
import 'package:foodherepos/views/orders/widgets/order_card.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:soundpool/soundpool.dart';

class OrdersView extends StatefulWidget {
  @override
  _OrdersViewState createState() => _OrdersViewState();
}

class _OrdersViewState extends State<OrdersView> {
  int _pages = 0;
  int _limit = 12;
  String _currentStatus = 'accepted';
  bool initialized = false;
  static int _currentPage = 0;
  PageController _controller =
      PageController(initialPage: _currentPage, viewportFraction: 0.9);
  List<Order> orders;
  List<Order> filteredOrders;
  int previousfilteredOrders = 0;
  bool isSearching = false;

  Soundpool _soundpool = Soundpool();
  Future<int> _soundId;
  int _alarmSoundStreamId;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _soundId = _loadSound();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    OrderService orderService = Provider.of<OrderService>(context);

    return Theme(
      data: ordersTheme,
      child: Stack(
        children: [
          Image.asset(
            "assets/images/background_2.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    stops: [0, 0.5],
                    colors: [Colors.black45, Colors.transparent])),
          ),
          Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              title: Provider.value(value: isSearching, child: isSearching ? TextField() : Text('Pedidos'),),
              actions: [
                IconButton(
                  icon: Icon(Icons.search, color: Colors.white,),
                  onPressed: () {
                    isSearching = !isSearching;
                  },
                )
              ],
            ),
            backgroundColor: Colors.transparent,
            body: SafeArea(
              child: StreamBuilder(
                stream: orderService.fetchOrdersAsStream(status: _currentStatus),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  print(snapshot.data);
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: LayoutBuilder(builder: (context, constraints) {
                          if (snapshot.connectionState ==
                              ConnectionState.active) {

                            if (snapshot.hasData) {
                              orders = snapshot.data.documents
                                  .map((doc) =>
                                      Order.fromMap(doc.data))
                                  .toList();

                              if (orders.isNotEmpty) {

                                if (orders.length > previousfilteredOrders && initialized == false) {
                                  int counter = 0;
                                  snapshot.data.documentChanges.forEach((change) {
                                    if (change.type == DocumentChangeType.added) {
                                      if (change.document.data['status'] == 'accepted') {
                                        print(change.document.data);
                                        counter ++;
                                      }
                                      if (counter > 0) {
                                        _playSound();
                                      }
                                    }
                                  });
                                }

                                previousfilteredOrders = orders.length;
                                initialized = true;

                                int totalOrders = orders.length;
                                _pages = (totalOrders / _limit).ceil();
                                int currentOrderIndex = 0;
                                List<Widget> pages = [];
                                int totalAdded = 0;
                                print(totalOrders);
                                for (var i = 1; i <= _pages; i++) {
                                  List<Widget> ordersPage = [];
                                  print('$i -------------------------');

                                  for (var a = currentOrderIndex;
                                  a < currentOrderIndex + _limit;
                                  a++) {
                                    if (totalAdded < totalOrders) {
                                      print('$a -> $totalAdded');
                                      ordersPage.add(
                                          OrderCard(order: orders[a]));
                                      totalAdded++;
                                    }
                                  }
                                  currentOrderIndex = totalAdded;
                                  pages.add(Wrap(
                                    children: ordersPage,
                                  ));
                                }



                                return Column(
                                  children: [
                                    Expanded(
                                        child: PageView(
                                            controller: _controller,
                                            children: pages)),
                                    Container(
                                      height: 100,
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: SmoothPageIndicator(
                                            controller: _controller,
                                            count: _pages,
                                            effect: ExpandingDotsEffect(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              } else {
                                print('No data');
                                return Center(
                                  child: Text(
                                    'No existen pedidos a mostrar',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                );
                              }

                            } else {
                              return Container();
                            }
                          } else {
                            print('loading data...');
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        }),
                      ),

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: _buildFilterButtons(orders: orders)
                          ),
                        ),

                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildFilterButtons({List<Order> orders}) {
    List<Widget> widgets = [];
    List<String> filters = [
      'accepted',
      'preparation',
      'ready',
      'delivering',
      'incidence',
      'finished',
      'cancelled'
    ];
    filters.forEach((status) {
      widgets.add(FilterButton(
        status: status,
        counter: 0,
        selected: status == _currentStatus,
        onTap: _onTapFilter,
      ));
    });
    return widgets;
  }

  _onTapFilter({String status}) {
    setState(() {
      initialized = status == 'accepted';
      _currentStatus = status;

    });
    SystemSound.play(SystemSoundType.click);
  }

  Future<int> _loadSound() async {
    var asset = await rootBundle.load("assets/sounds/that-was-quick.mp3");
    return await _soundpool.load(asset);
  }

  Future<void> _playSound() async {
    var _alarmSound = await _soundId;
    _alarmSoundStreamId = await _soundpool.play(_alarmSound);
  }
}
