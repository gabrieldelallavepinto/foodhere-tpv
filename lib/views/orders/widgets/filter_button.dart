import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/order.dart';
import 'package:foodherepos/core/models/status_style.dart';

class FilterButton extends StatelessWidget {
  final String status;
  final int counter;
  final bool selected;
  final Function onTap;

  FilterButton({@required this.status, @required this.counter, @required this.onTap, @required this.selected});

  @override
  Widget build(BuildContext context) {

    List<StatusStyle> filterStatus = [
      new StatusStyle(
          status: 'accepted',
          label: 'ACEPTADO',
          textColor: Colors.white,
          color: Color.fromRGBO(77, 129, 187, 1)),
      new StatusStyle(
          status: 'preparation',
          label: 'EN COCINA',
          textColor: Colors.white,
          color: Color.fromRGBO(255, 150, 34, 1)),
      new StatusStyle(
          status: 'ready',
          label: 'PREPARADO',
          textColor: Colors.white,
          color: Color.fromRGBO(16, 194, 173, 1)),
      new StatusStyle(
          status: 'delivering',
          label: 'EN RUTA',
          textColor: Colors.white,
          color: Color.fromRGBO(157, 61, 225, 1)),
      new StatusStyle(
          status: 'incidence',
          label: 'INCIDENCIAS',
          textColor: Colors.black,
          color: Color.fromRGBO(255, 250, 16, 1)),
      new StatusStyle(
          status: 'finished',
          label: 'FINALIZADOS',
          textColor: Colors.white,
          color: Color.fromRGBO(45, 168, 106, 1)),
      new StatusStyle(
          status: 'cancelled',
          label: 'CANCELADOS',
          textColor: Colors.white,
          color: Color.fromRGBO(197, 0, 0, 1)),
    ];

    StatusStyle currentStatus = filterStatus.firstWhere((e) => e.status == status);

    return Badge(
      position: BadgePosition.topRight(),
      animationType: BadgeAnimationType.fade,
      showBadge: counter > 0,
      badgeContent: Text('$counter'),
      padding: EdgeInsets.all(8),
      badgeColor: Colors.white,
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 150, minWidth: 150, minHeight: 45),
        child: FlatButton(
          child: Text(
            currentStatus.label,
            overflow: TextOverflow.ellipsis,
          ),
          textColor: selected == true ? Colors.black54 : Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
            side: BorderSide(color: currentStatus.color),
          ),
          color: selected == true ? currentStatus.color: Colors.white10,
          onPressed: () => onTap(status: currentStatus.status),
        ),
      ),
    );
  }
}



