import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/order.dart';
import 'package:foodherepos/core/models/status_style.dart';

class OrderCard extends StatefulWidget {
  final Order order;

  OrderCard({this.order});

  @override
  _OrderCardState createState() => _OrderCardState();
}

class _OrderCardState extends State<OrderCard> {
  double cardPadding = 4;
  double numberRows = 4;

  static List<StatusStyle> filterStatus = [
    new StatusStyle(
        status: 'accepted',
        label: 'ACEPTADO',
        textColor: Colors.white,
        color: Color.fromRGBO(77, 129, 187, 1)),
    new StatusStyle(
        status: 'preparation',
        label: 'EN COCINA',
        textColor: Colors.white,
        color: Color.fromRGBO(255, 150, 34, 1)),
    new StatusStyle(
        status: 'ready',
        label: 'PREPARADO',
        textColor: Colors.white,
        color: Color.fromRGBO(16, 194, 173, 1)),
    new StatusStyle(
        status: 'delivering',
        label: 'EN RUTA',
        textColor: Colors.white,
        color: Color.fromRGBO(157, 61, 225, 1)),
    new StatusStyle(
        status: 'incidence',
        label: 'INCIDENCIAS',
        textColor: Colors.black,
        color: Color.fromRGBO(255, 250, 16, 1)),
    new StatusStyle(
        status: 'finished',
        label: 'FINALIZADOS',
        textColor: Colors.white,
        color: Color.fromRGBO(45, 168, 106, 1)),
    new StatusStyle(
        status: 'cancelled',
        label: 'CANCELADOS',
        textColor: Colors.white,
        color: Color.fromRGBO(197, 0, 0, 1)),
    new StatusStyle(
        status: 'waiting_sp_validation',
        label: 'CANCELADOS',
        textColor: Colors.white,
        color: Color.fromRGBO(197, 0, 0, 1)),
  ];

  @override
  Widget build(BuildContext context) {
    StatusStyle status =
        filterStatus.firstWhere((e) => e.status == widget.order.status);

    return Padding(
      padding: EdgeInsets.all(cardPadding),
      child: ConstrainedBox(
          constraints: BoxConstraints(
            maxWidth: (MediaQuery.of(context).size.width * 0.9 / numberRows) -
                cardPadding * 2,
          ),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              children: [
                OrderCardHeader(status: status, order: widget.order),
                OrderCardBody(status: status, order: widget.order),
                OrderCardFooter(status: status, order: widget.order)
              ],
            ),
          )),
    );
  }
}

class OrderCardHeader extends StatelessWidget {
  final StatusStyle status;
  final Order order;

  OrderCardHeader({this.status, this.order});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status.color,
        borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(8.0),
            topRight: const Radius.circular(8.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '#212',
                    style: TextStyle(
                        color: status.textColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  Text(
                    '10-05-2020 11:35:00',
                    style: TextStyle(
                      color: status.textColor,
                    ),
                  )
                ],
              ),
            ),
            Text(
              '10:38',
              style: TextStyle(
                  color: status.textColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            Icon(
              Icons.timer,
              color: status.textColor,
            )
          ],
        ),
      ),
    );
  }
}

class OrderCardBody extends StatelessWidget {
  final StatusStyle status;
  final Order order;

  OrderCardBody({this.status, this.order});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: 80
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              order.client.phone,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Text(
              '${order.client.firstName} ${order.client.lastName}',
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
            ),
            order.type == 'delivery'
                ? Text(
                    order.deliveryAddress.formattedAddress,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.normal),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}

class OrderCardFooter extends StatelessWidget {
  final StatusStyle status;
  final Order order;

  OrderCardFooter({this.status, this.order});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(239, 239, 239, 1),
        borderRadius: BorderRadius.only(
            bottomLeft: const Radius.circular(8.0),
            bottomRight: const Radius.circular(8.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.all(
                  const Radius.circular(4.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  'APP',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              width: 4,
            ),
            Container(
              decoration: BoxDecoration(
                color: _getColorType(order.type),
                borderRadius: BorderRadius.all(
                  const Radius.circular(4.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  _getLabelType(order.type),
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Color _getColorType(String type) {
    switch (type) {
      case 'delivery':
        return Colors.green;
        break;
      case 'collect':
        return Colors.orange;
        break;
      case 'in_store':
        return Colors.purple;
        break;
      default:
        return Colors.red;
        break;
    }
  }

  String _getLabelType(String type) {
    switch (type) {
      case 'delivery':
        return 'DOMICILIO';
        break;
      case 'collect':
        return 'A RECOGER';
        break;
      case 'in_store':
        return 'A TOMAR EN TIENDA';
        break;
      default:
        return'DESCONOCIDO';
        break;
    }
  }
}
