import 'package:flutter/material.dart';

class HomeFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/logo_foodhere.png",
            width: 120,
            fit: BoxFit.fitHeight,
          ),
          Text(
            '© 2020 FoodHere Solutions, s.l. Todos los derechos reservados',
            style: TextStyle(
                color: Colors.white70,
                fontSize: 12
            ),
          )
        ],
      ),
    );
  }
}
