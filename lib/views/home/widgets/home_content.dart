import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/home/widgets/home_menu_item.dart';

class HomeContent extends StatefulWidget {

  final List<MenuItem> menuItems;
  final Function onTapItem;

  HomeContent({@required this.menuItems, @required this.onTapItem});

  @override
  _HomeContentState createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.center,
      children: _generateMenu(),
    );
  }



  List<Widget> _generateMenu() {
    List<Widget> widgets = [];
    widget.menuItems.forEach((item) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: HomeMenuItem(item: item, onTapItem: _onTapItem,)
        ),
      );
    });

    return widgets;
  }

  _onTapItem({MenuItem item}) {
    Navigator.of(context).pushNamed(item.route);
  }
}
