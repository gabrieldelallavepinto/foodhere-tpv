import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodherepos/views/home/widgets/clock/digital_clock.dart';

class HomeHeader extends StatefulWidget {

  @override
  _HomeHeaderState createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(),
          ),
          Expanded(
            child: Image.asset(
              "assets/images/logo.png",
              height: 90,
              fit: BoxFit.fitHeight,
            ),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}
