import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';

class HomeMenuItem extends StatelessWidget {

  final MenuItem item;
  final Function onTapItem;
  HomeMenuItem({@required this.item, @required this.onTapItem});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTapItem(item: item);
      },
      child: Container(
        height: 100,
        width: 90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 70,
              width: 70,
              decoration: BoxDecoration(
                  color: item.color,
                  borderRadius:
                  new BorderRadius.all(const Radius.circular(10.0))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Image.asset(
                    'assets/icons/${item.icon}.png',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              item.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
