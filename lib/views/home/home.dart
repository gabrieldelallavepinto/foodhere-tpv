import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/core/models/order.dart';
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/order_service.dart';
import 'package:foodherepos/core/services/printer_service.dart';
import 'package:foodherepos/views/home/home_theme.dart';
import 'package:foodherepos/views/home/widgets/clock/digital_clock.dart';
import 'package:foodherepos/views/home/widgets/home_content.dart';
import 'package:foodherepos/views/home/widgets/home_footer.dart';
import 'package:foodherepos/views/home/widgets/home_header.dart';
import 'package:foodherepos/views/home/widgets/home_left.dart';
import 'package:foodherepos/views/home/widgets/home_right.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final authService = Provider.of<AuthService>(context);

    return Theme(
      data: homeTheme,
      child: Stack(fit: StackFit.expand, children: <Widget>[
        Image.asset(
          "assets/images/background_2.jpg",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    HomeLeft(),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(flex: 2, child: HomeHeader()),
                          Expanded(
                            flex: 8,
                            child: Center(
                                child: HomeContent(menuItems: _getFakeMenu())),
                          ),
                        ],
                      ),
                    ),
                    HomeRight()
                  ],
                ),
              ),
              HomeFooter(),
              RaisedButton(
                color: Colors.pink,
                child: Text(
                  'Cerrar Sesión',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  dynamic result = await authService.signOut();
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }

  static List<MenuItem> _getFakeMenu() {
    List<MenuItem> items = [
      new MenuItem(
          name: 'Pedidos',
          icon: 'receipt',
          route: 'orders',
          color: Colors.black12),
      new MenuItem(
          name: 'Configuración',
          icon: 'setting',
          route: 'setting',
          color: Colors.black12),
    ];

    return items;
  }
}
