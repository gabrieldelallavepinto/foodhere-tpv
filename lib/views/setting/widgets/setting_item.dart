import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/setting/setting_theme.dart';

class SettingItem extends StatelessWidget {
  final MenuItem item;
  final Function onTapItem;

  SettingItem({@required this.item, @required this.onTapItem});

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width);
    return GestureDetector(
      onTap: () {
        onTapItem(item: item);
      },
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: 100,
          minHeight: 100,
          maxWidth: 280,
          minWidth: 280
        ),
        child: Container(
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.2),
              border: Border.all(color: Color.fromRGBO(255, 255, 255, 0.2), style: BorderStyle.solid, width: 1),
              borderRadius:
              new BorderRadius.all(const Radius.circular(10.0))),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 60,
                  height: 60,
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: Image.asset(
                      'assets/icons/${item.icon}.png',
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          item.name,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: settingTheme.textTheme.headline4,
                        ),
                        SizedBox(height:5),
                        Expanded(
                          child: Text(
                            item.description,
                            textAlign: TextAlign.left,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: settingTheme.textTheme.caption,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
