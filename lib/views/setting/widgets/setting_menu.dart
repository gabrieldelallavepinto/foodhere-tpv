import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/setting/setting_theme.dart';

class SettingMenu extends StatefulWidget {

  final MenuItem item;
  final Function onTapItem;
  final String selected;

  SettingMenu({@required this.item, @required this.onTapItem, @required this.selected});

  @override
  _SettingMenuState createState() => _SettingMenuState();
}

class _SettingMenuState extends State<SettingMenu> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.white54, height: 0,
      ),
      itemBuilder: (BuildContext context, int index) {
        return Theme(
          data: ThemeData(
            primaryColor: Colors.black87
          ),
          child: Ink(
              color: widget.selected == widget.item.settingOptions[index].route ? Colors.white24 : Colors.transparent,
              child: ListTile(
                title: Text(widget.item.settingOptions[index].name, style: settingOptionsTheme.textTheme.subtitle1,),
                leading: Image.asset('assets/icons/${widget.item.settingOptions[index].icon}.png'),
                subtitle: Text(widget.item.settingOptions[index].description),
                trailing: widget.selected == widget.item.settingOptions[index].route ? Icon(Icons.keyboard_arrow_right) : SizedBox(),
                selected: widget.selected == widget.item.settingOptions[index].route,
                onTap: (){
                  widget.onTapItem(option: widget.item.settingOptions[index]);
                },
              ),
          ),
        );
      },
      itemCount: widget.item.settingOptions.length,
    );
  }
}
