import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/setting/devices/devices_printer.dart';
import 'package:foodherepos/views/setting/devices/devices_screen.dart';
import 'package:foodherepos/views/setting/setting_theme.dart';
import 'package:foodherepos/views/setting/widgets/setting_menu.dart';

class SettingOptionsView extends StatefulWidget {
  final MenuItem item;

  SettingOptionsView({@required this.item});

  @override
  _SettingOptionsViewState createState() => _SettingOptionsViewState();
}

class _SettingOptionsViewState extends State<SettingOptionsView> {
  String _selected;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selected = widget.item.settingOptions[0].route;
    print(_selected);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: settingOptionsTheme,
      child: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/background_1.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Positioned(
            top: 0,
            left: 0,
            height: MediaQuery.of(context).size.height,
            child:  Container(
              color: Color.fromRGBO(0, 0, 0, 0.2),
              width: MediaQuery.of(context).size.width * 0.25,
            ),
          ),

          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Text(widget.item.name),
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            body: Row(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: SettingMenu(
                      item: widget.item,
                      selected: _selected,
                      onTapItem: _onTapItem,
                    )),
                Expanded(
                  child: _getSettingsView(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getSettingsView() {
    switch (_selected) {
      case 'printers':
        return DevicesPrinterView();
        break;
      case 'screens':
        return DevicesScreenView();
        break;
    }

  }

  void _onTapItem({SettingOption option}) {
    setState(() {
      _selected = option.route;
    });
  }
}
