import 'package:flutter/material.dart';
import 'package:foodherepos/views/setting/devices/widgets/printerItem.dart';

class DevicesPrinterView extends StatefulWidget {
  @override
  _DevicesPrinterViewState createState() => _DevicesPrinterViewState();
}

class _DevicesPrinterViewState extends State<DevicesPrinterView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
        backgroundColor: Colors.blueGrey,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text('Impresoras',
                      style: Theme.of(context).textTheme.headline5),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Wrap(
                  children: [
                    DevicePrinterItem(enabled: false, type: 'bluetooth', name: 'Impresora cocina 1', data: '12312HB8NHH'),
                    DevicePrinterItem(enabled: true, type: 'wifi', name: 'Impresora cocina 2', data: '12312HB8NHH'),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
