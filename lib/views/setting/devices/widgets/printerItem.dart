import 'package:flutter/material.dart';

class DevicePrinterItem extends StatelessWidget {

  final String name;
  final String data;
  final bool enabled;
  final String type;
  DevicePrinterItem({this.name, this.enabled, this.data, this.type});

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: ConstrainedBox(
          constraints: BoxConstraints(
              minWidth: 240, minHeight: 100, maxWidth: 240),
          child: Container(
            decoration: BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                border: Border.all(color: enabled != true ? Colors.red : Color.fromRGBO(255, 255, 255, 0.6), style: BorderStyle.solid, width: 1),
                borderRadius:
                new BorderRadius.all(const Radius.circular(10.0))),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 60,
                      child: Image.asset('assets/icons/printer_${type}${enabled != true ? '_disabled':''}.png')),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w800),
                        ),
                        Text(
                          '${type == 'wifi' ? 'IP:' : 'MAC:'}: ${data}',
                          style: TextStyle(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
