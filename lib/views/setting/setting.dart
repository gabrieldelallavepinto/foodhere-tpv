import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/setting/setting_theme.dart';
import 'package:foodherepos/views/setting/widgets/setting_item.dart';

class SettingView extends StatefulWidget {
  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {

  List<MenuItem> _settingItems = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _settingItems = _getFakeSettingItems();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: settingTheme,
      child: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/background_4.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,

          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Text('Configuración'),
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    children: _generateItems(),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  static List<MenuItem> _getFakeSettingItems() {
    List<MenuItem> items = [
      new MenuItem(
          name: 'Dispositivos',
          description: 'Impresoras, scanners, pantallas, bluetooth, ...',
          icon: 'devices',
          route: 'orders',
          color: Colors.black12,
        settingOptions: [
          new SettingOption(
            name: 'Impresoras',
            description: 'Configura las impresoras',
            icon: 'printers',
            route: 'printers'
          ),
          new SettingOption(
              name: 'Pantallas',
              description: 'Configura tus pantallas',
              icon: 'screens',
              route: 'screens'
          ),
        ]
      ),

    ];

    return items;
  }

  List<Widget> _generateItems() {
    List<Widget> widgets = [];
    _settingItems.forEach((item) {
      widgets.add(
        Padding(
            padding: const EdgeInsets.all(8.0),
            child: SettingItem(item: item, onTapItem: _onTapItem,)
        ),
      );
    });

    return widgets;
  }

  _onTapItem({MenuItem item}) {
    Navigator.of(context).pushNamed('settingOptions', arguments: item);
  }
}
