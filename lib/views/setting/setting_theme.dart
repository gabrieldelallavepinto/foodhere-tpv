import 'package:flutter/material.dart';

final ThemeData settingTheme = new ThemeData(
  primaryColor: Color.fromARGB(255, 229, 30, 37),
  accentColor: Color.fromARGB(255, 0, 152, 77),
  textTheme: TextTheme(

    bodyText2: TextStyle(color: Colors.white),
    headline4: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.w500),
    caption: TextStyle(color: Colors.white60,),
  ),
);

final ThemeData settingOptionsTheme = new ThemeData(
  textTheme: TextTheme(
    bodyText2: TextStyle(color: Colors.black87),
    subtitle1: TextStyle(color: Colors.black87, fontWeight: FontWeight.w500),
  )
);
