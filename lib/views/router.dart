import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/menuItem.dart';
import 'package:foodherepos/views/home/home.dart';
import 'package:foodherepos/views/orders/orders.dart';
import 'package:foodherepos/views/setting/setting.dart';
import 'package:foodherepos/views/setting/setting_options.dart';
import 'package:foodherepos/wrapper.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'wrapper':
        return MaterialPageRoute(builder: (_) => Wrapper());
      case 'home':
        return MaterialPageRoute(builder: (_) => HomeView());
      case 'orders':
        return MaterialPageRoute(builder: (_) => OrdersView());
      case 'setting':
        return MaterialPageRoute(builder: (_) => SettingView());
      case 'settingOptions':
        MenuItem item = settings.arguments as MenuItem;
        return MaterialPageRoute(builder: (_) => SettingOptionsView(item: item));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
