
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/order_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => BusinessService());
  locator.registerLazySingleton(() => OrderService());
  locator.registerLazySingleton(() => AuthService());
}