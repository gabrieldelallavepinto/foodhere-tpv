import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:foodherepos/core/models/order.dart';
import 'package:foodherepos/core/services/auth_service.dart';
import 'package:foodherepos/core/services/store_service.dart';
import 'package:foodherepos/shared/loading.dart';
import 'package:foodherepos/views/home/home.dart';
import 'package:foodherepos/views/login/login.dart';
import 'package:foodherepos/views/shared/update_app.dart';
import 'package:provider/provider.dart';

import 'package:foodherepos/core/models/business.dart';
import 'package:foodherepos/core/services/business_service.dart';
import 'package:foodherepos/core/services/order_service.dart';
import 'package:foodherepos/views/theme.dart';

import 'core/models/store.dart';
import 'core/services/business_service.dart';
import 'core/services/printer_service.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    // Observamos los pedidos

//    OrderService().getOrdersStream('zLIzDEFA1hVwXKWLCxLR', queries).listen(
//      (data) {
//        data.documents.forEach((doc) {
//          print(doc.data);
//
//          Order order = Order.fromMap(doc.data);
//          final printOrder = OrderService().printOrder(order);
//        });
//      },
//    );

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        EdgeAlert.show(context,
            title: message['notification']['title'],
            description: message['notification']['body'],
            gravity: EdgeAlert.TOP,
            backgroundColor: theme.accentColor);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print('carga el wrapper');

    final authService = Provider.of<AuthService>(context);
    final businessService = Provider.of<BusinessService>(context);
    final storeService = Provider.of<StoreService>(context);
    final orderService = Provider.of<OrderService>(context);

    if (authService.user == null){
      return LoginView();
    } else {
      return FutureBuilder(
        future: Future.wait([
          businessService.getBusiness(authService.employee.businessId),
          storeService.getStore(authService.employee.businessId, authService.employee.storeId),
        ]),
        builder: (context, snapshot) {

          if (snapshot.connectionState == ConnectionState.done) {
            print('se ha detectado la empresa:');
            businessService.business = snapshot.data[0];
            print(businessService.business.toJson());

            print('se ha detectado la tienda:');
            DocumentSnapshot doc = snapshot.data[1];
            storeService.store = Store.fromMap(doc.data);

            print('datos de los pedidos:');

            OrderService().getOrdersStream(businessService.business.id, storeId: storeService.store.id).listen((snapshot) {
              snapshot.documents.forEach((doc) async {

                Order order = Order.fromMap(doc.data);
                if(order.printed == false){
                  final ticket = await order.toTicket();
                  final prueba = await PrinterService().printTicket(ticket, '192.168.1.100');
                  // value == 1 impreso ; value == 2 no se puede imprimir
                  print(prueba.value);
                }
              });
            });

            return HomeView();
          } else {
            return Loading();
          }

        },
      );
    }

  }
}

class Merged {
  Business business;

  Merged({this.business});
}
